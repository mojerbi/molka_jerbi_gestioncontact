﻿using Data;
using System;
using System.Collections.Generic;
using System.IO;

namespace DotNetProject
{
    class Program
    {
        static void Main(string[] args)
        {
            // Création d'un dossier racine
            Folder root = new Folder();
            root.Name = "Root";
            root.CreationDate = DateTime.Now;
            root.ModificationDate = DateTime.Now;
            List<ContactManagement> gestionContacts = new List<ContactManagement>();
            List<ContactManagement> gestionContactsUpload = new List<ContactManagement>();
            ContactManagement gestionContact = new ContactManagement { Folder = root, Contacts = new List<Contact>() };
            gestionContacts.Add(gestionContact);

            // Affichage du menu à l'utilisateur
            string Choix;
            bool Menu = false;
            Console.WriteLine(@"/*-------------Menu---------\*");
            Console.WriteLine(@"/*1- Afficher               \*");
            Console.WriteLine(@"/*2- Charger                \*");
            Console.WriteLine(@"/*3- Enregistrer            \*");
            Console.WriteLine(@"/*4- AjouterDossier         \*");
            Console.WriteLine(@"/*5- AjouterContact         \*");
            Console.WriteLine(@"/*6- Sortir                 \*");
            Console.WriteLine(@"/*--------------------------\*");

            while (!Menu)
            {
                // Lecture de l'entrée de l'utilisateur
                Choix = Console.ReadLine();
                Choix = (!string.IsNullOrEmpty(Choix) ? Choix.ToLower() : string.Empty);
                string[] tab = Choix.Split(' ', StringSplitOptions.None);

                // Appel de la fonction appropriée pour chaque choix
                switch (tab[0])
                {
                    case "afficher":
                        Function.Display(gestionContacts);
                        break;

                    case "ajouterdossier":
                        if (tab.Length > 1)
                        {
                            Function.AddFolder(gestionContacts, tab[1]);
                        }
                        else
                        {
                            Console.WriteLine("Spécifiez le nom du dossier.");
                        }
                        break;

                    case "ajoutercontact":
                        if (tab.Length > 5)
                        {
                            Function.AddContact(gestionContacts, tab);
                        }
                        else
                        {
                            Console.WriteLine("Spécifiez les informations du contact.");
                        }
                        break;

                    case "charger":
                        string NameUser = Environment.UserName;
                        Console.WriteLine("Entrez le type de chargement souhaité : ");
                        Console.WriteLine(" 1 : Xml ");
                        Console.WriteLine(" 2 : Binaire ");
                        int? type = int.Parse(Console.ReadLine());

                        if (type == 1)
                        {
                            gestionContactsUpload = (List<ContactManagement>)(Function.XmlDeserialize(typeof(List<ContactManagement>), @$"C:\Users\{NameUser}\Documents\ContactManager2.db"));
                            if (gestionContactsUpload != null)
                            {
                                gestionContacts = gestionContactsUpload;
                                Console.WriteLine("L'objet est chargé.");
                            }

                        }
                        else if (type == 2)
                        {
                            gestionContactsUpload = (List<ContactManagement>)Function.BinaryDeserialize(@$"C:\Users\{NameUser}\Documents\ContactManager1.db");
                            if (gestionContactsUpload != null)
                            {
                                gestionContacts = gestionContactsUpload;
                                Console.WriteLine("L'objet est chargé.");
                            }
                        }
                        else
                        {
                            Console.WriteLine("Ce choix n'existe pas.");
                        }

                        break;

                    case "enregistrer":
                        string UserName = Environment.UserName;
                        Console.WriteLine("Entrez le type d'enregistrement souhaité : ");
                        Console.WriteLine(" 1 : Xml ");
                        Console.WriteLine(" 2 : Binaire ");
                        int? intType = int.Parse(Console.ReadLine());

                        if (intType == 1)
                        {
                            Function.XmlSerialize(typeof(List<ContactManagement>), gestionContacts, @$"C:\Users\{UserName}\Documents\ContactManager2.db");
                        }
                        else
                        {
                            Function.BinarySerialize(gestionContacts, @$"C:\Users\{UserName}\Documents\ContactManager.db");
                        }
                        Console.WriteLine(@"Fichier 'C:\Users\molka\Documents\ContactManager.db' enregistré.");
                        break;

                    case "sortir":
                        Console.WriteLine("Au revoir !");
                        Menu = true;
                        break;

                    default:
                        Console.WriteLine("Instruction inconnue.");
                        break;
                }
            }
        }
    }
}
