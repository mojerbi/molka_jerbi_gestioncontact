#pragma warning disable SYSLIB0011
using System;
using System.Xml.Serialization;
using System.Security.Cryptography;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
using Data;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DotNetProject
{
    public static class Function
    {
        // Fonction utilisée pour l'affichage des données
        public static void Display(List<ContactManagement> contactManagements)
        {
            int j = 0;
            foreach (var c in contactManagements)
            {
                j++;
                Console.WriteLine(((c.Folder != null) ? c.Folder.ToString() : " Aucun dossier créé "));

                if (c.Contacts != null)
                {
                    foreach (var contact in c.Contacts)
                    {
                        for (int i = 0; i < j - 1; i++)
                        {
                            Console.Write(" ");
                        }
                        Console.WriteLine(contact.ToString());
                    }
                }

                for (int i = 0; i < j; i++)
                {
                    Console.Write(" ");
                }
            }
        }

        // Fonction utilisée pour ajouter un nouveau dossier
        public static void AddFolder(List<ContactManagement> contactManagements, string folderName)
        {
            if (folderName != "")
            {
                // Création d'un nouveau dossier avec le nom spécifié et les dates de création et de modification actuelles
                ContactManagement newFolder = new ContactManagement { Folder = new Folder { Name = folderName, CreationDate = DateTime.Now, ModificationDate = DateTime.Now }, Contacts = new List<Contact>() };
                // Ajout du nouveau dossier à la liste des dossiers de gestion de contact
                contactManagements.Add(newFolder);
                int pos = contactManagements.Count;
                Console.Write("Dossier '" + folderName + "' ajouté sous '" + contactManagements.ElementAt(pos - 2).Folder.Name + "' en position " + (pos - 1));
            }
            else
            {
                Console.WriteLine("Spécifiez le nom du dossier.");
            }
        }

        // Fonction utilisée pour la vérification du format de l'e-mail
        public static bool IsEmail(string email)
        {
            // Expression régulière pour vérifier le format de l'e-mail
            const string em = @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
            if (email != null) return Regex.IsMatch(email, em);
            else return false;
        }

        // Fonction utilisée pour ajouter un nouveau contact
        public static void AddContact(List<ContactManagement> contactManagements, string[] tab)
        {
            int pos = contactManagements.Count;
            string[] links = new string[4] { "ami", "collègue", "relation", "raison" };

            if (links.Contains(tab[5]) && IsEmail(tab[4]))
            {
                // Conversion du lien du contact en une énumération
                ContactLink _link = (ContactLink)Enum.Parse(typeof(ContactLink), tab[5]);

                // Création d'un nouveau contact avec les données spécifiées
                Contact contact = new Contact
                {
                    LastName = tab[1].Substring(0, 1).ToUpper() + tab[1].Substring(1),
                    FirstName = tab[2].Substring(0, 1).ToUpper() + tab[2].Substring(1),
                    Company = tab[3].Substring(0, 1).ToUpper() + tab[3].Substring(1),
                    Email = tab[4],
                    CreationDate = DateTime.Now,
                    ModificationDate = DateTime.Now,
                    Link = _link
                };

                // Ajout du nouveau contact à la liste de contacts du dossier de gestion de contact approprié
                contactManagements[pos - 1].Contacts.Add(contact);
                Console.Write("Contact '" + contact.LastName + "' ajouté sous '" + contactManagements.ElementAt(pos - 1).Folder.Name + "' en position " + (pos));
            }
            else
            {
                if (IsEmail(tab[4]))
                {
                    Console.WriteLine("Le lien est incorrect.");
                    Console.WriteLine("Liens existants : { ami, collègue, relation, raison }");
                }
                else
                {
                    Console.WriteLine("L'e-mail est incorrect.");
                }
            }
        }

        // Fonction utilisée pour sérialiser les données au format binaire
        public static void BinarySerialize(object data, string path)
        {
            Console.WriteLine("Enregistrement du fichier '" + path + "'...");
            try
            {
                FileStream file;
                BinaryFormatter formatter = new BinaryFormatter();
                if (File.Exists(path)) File.Delete(path);

                // Configuration du fournisseur de chiffrement pour la sécurité
                DESCryptoServiceProvider cryptoProvider = new DESCryptoServiceProvider();
                cryptoProvider.Key = ASCIIEncoding.ASCII.GetBytes("19991810");
                cryptoProvider.IV = ASCIIEncoding.ASCII.GetBytes("19991810");
                file = new FileStream(path, FileMode.CreateNew);
                CryptoStream cryptoStream = new CryptoStream(file, cryptoProvider.CreateEncryptor(), CryptoStreamMode.Write);

                // Sérialisation des données dans le flux de chiffrement
                formatter.Serialize(cryptoStream, data);
                cryptoStream.Close();
            }
            catch (Exception ex)
            {
                // Gestion des exceptions lors de la sérialisation
                Console.WriteLine("Erreur lors de la sérialisation : " + ex.Message);
            }
        }

        // Fonction utilisée pour désérialiser les données binaires
        public static object BinaryDeserialize(string path)
        {
            List<ContactManagement> objectList = null;
            DESCryptoServiceProvider cryptoProvider = new DESCryptoServiceProvider();
            cryptoProvider.Key = ASCIIEncoding.ASCII.GetBytes("19991810");
            cryptoProvider.IV = ASCIIEncoding.ASCII.GetBytes("19991810");
            FileStream fileStream;
            BinaryFormatter formatter = new BinaryFormatter();
            string password;
            int counter = 0;

            if (File.Exists(path))
            {
                Console.WriteLine("Entrez un mot de passe :");
                do
                {
                    password = Console.ReadLine();
                    counter++;
                } while (password != "Cs" && counter < 3);

                if (counter < 3)
                {
                    CryptoStream cryptoStream = new CryptoStream(File.Open(path, FileMode.Open), cryptoProvider.CreateDecryptor(), CryptoStreamMode.Read);
                    // Désérialisation des données à partir du flux de chiffrement
                    objectList = (List<ContactManagement>)formatter.Deserialize(cryptoStream);
                    cryptoStream.Close();
                }
                else
                {
                    File.Delete(path);
                    Console.WriteLine("Fichier supprimé...");
                }

                return objectList;
            }
            else
            {
                Console.WriteLine("Le fichier n'existe pas.");
                return objectList;
            }
        }

        // Fonction utilisée pour sérialiser les données au format XML
        public static void XmlSerialize(Type dataType, object data, string path)
        {
            Console.WriteLine("Enregistrement du fichier '" + path + "'...");
            XmlSerializer xmlSerializer = new XmlSerializer(dataType);
            if (File.Exists(path)) File.Delete(path);

            DESCryptoServiceProvider cryptoProvider = new DESCryptoServiceProvider();
            cryptoProvider.Key = ASCIIEncoding.ASCII.GetBytes("19991810");
            cryptoProvider.IV = ASCIIEncoding.ASCII.GetBytes("19991810");
            FileStream stream = new FileStream(path, FileMode.CreateNew);
            CryptoStream cryptoStream = new CryptoStream(stream, cryptoProvider.CreateEncryptor(), CryptoStreamMode.Write);

            // Sérialisation des données dans le flux de chiffrement
            xmlSerializer.Serialize(cryptoStream, data);
            cryptoStream.Close();
        }

        // Fonction utilisée pour désérialiser les données XML
        public static object XmlDeserialize(Type dataType, string path)
        {
            object obj = null;
            XmlSerializer xmlSerializer = new XmlSerializer(dataType);
            DESCryptoServiceProvider cryptoProvider = new DESCryptoServiceProvider();
            cryptoProvider.Key = ASCIIEncoding.ASCII.GetBytes("19991810");
            cryptoProvider.IV = ASCIIEncoding.ASCII.GetBytes("19991810");
            string password;
            int counter = 0;

            if (File.Exists(path))
            {
                Console.WriteLine("Entrez un mot de passe :");
                do
                {
                    password = Console.ReadLine();
                    counter++;
                } while (password != "Cs" && counter < 3);

                if (counter < 3)
                {
                    CryptoStream cryptoStream = new CryptoStream(File.Open(path, FileMode.Open), cryptoProvider.CreateDecryptor(), CryptoStreamMode.Read);
                    // Désérialisation des données à partir du flux de chiffrement
                    obj = xmlSerializer.Deserialize(cryptoStream);
                    cryptoStream.Close();
                }
                else
                {
                    File.Delete(path);
                    Console.WriteLine("Fichier supprimé...");
                }
                return obj;
            }
            else
            {
                Console.WriteLine("Le fichier n'existe pas.");
                return obj;
            }
        }
    }
}
