namespace Data
{
    // Repr�sente le type de relation avec un contact
    public enum ContactLink
    {
        // Repr�sente une relation d'ami
        ami,

        // Repr�sente une relation de coll�gue
        coll�gue,

        // Repr�sente une relation de relation
        relation,

        // Repr�sente une relation de raison
        raison
    }
}
