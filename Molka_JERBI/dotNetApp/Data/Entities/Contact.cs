namespace Data
{
    // Représente un contact
    [Serializable]
    public class Contact
    {
        // Obtient ou définit le nom de famille du contact
        public string LastName { get; set; } = string.Empty;

        // Obtient ou définit le prénom du contact
        public string FirstName { get; set; } = string.Empty;

        // Obtient ou définit l'adresse e-mail du contact
        public string Email { get; set; } = string.Empty;

        // Obtient ou définit la société du contact
        public string Company { get; set; } = string.Empty;

        // Obtient ou définit le lien de relation avec le contact
        public ContactLink Link { get; set; }

        // Obtient ou définit la date de création du contact
        public DateTime CreationDate { get; set; }

        // Obtient ou définit la date de modification du contact
        public DateTime ModificationDate { get; set; }

        // Remplace la méthode ToString pour fournir une représentation de chaîne personnalisée du contact
        public override string ToString()
        {
            return "| " + LastName + " , " + FirstName + " , " + Company + " , " + Email + " , " + Link;
        }
    }
}
