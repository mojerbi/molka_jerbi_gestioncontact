namespace Data
{
    [Serializable]
    // Repr�sente la gestion des contacts
    public class ContactManagement
    {
        // Repr�sente le dossier associ� � la gestion des contacts
        public Folder? Folder { get; set; }

        // Repr�sente la liste des contacts
        public List<Contact>? Contacts { get; set; }
    }
}
