namespace Data
{
    [Serializable]
    // Représente un dossier
    public class Folder
    {
        // Représente le nom du dossier
        public string Name { get; set; } = string.Empty;

        // Représente la date de création du dossier
        public DateTime CreationDate { get; set; }

        // Représente la date de modification du dossier
        public DateTime ModificationDate { get; set; }

        // Remplace la méthode ToString pour fournir une représentation de chaîne personnalisée
        public override string ToString()
        {
            // Retourne la chaîne formatée contenant le nom du dossier et les dates
            return " | " + Name + " ,  " + CreationDate + "  ";
        }
    }
}
